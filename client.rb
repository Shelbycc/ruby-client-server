require 'socket'
require 'benchmark'

class Client
   def initialize(socket)
      @socket = socket
      @request_object = send_request
      @response_object = listen_response

      @request_object.join # will send the request to server
      @response_object.join # will receive response from server
   end

   def send_request
      puts "Please enter your username to establish a connection..."
      Thread.new do
         loop do
            message = $stdin.gets.chomp
            @socket.puts message
            case message.split(" ")[0]
            when "UPLOAD"
            when "DOWNLOAD"
               time = Benchmark.realtime do 
                  File.open("output", 'wb') do |file| 
		     size = client.gets.chomp.to_i
                     while chunk = @socket.read(size) 
                        file.write(chunk) 
                     end 
                  end 
               end
            end
         end
      end
   end

   def listen_response
      Thread.new do
         loop do
            response = @socket.gets.chomp
            if response.eql?'quit'
               @socket.close
            else
               puts "#{response}"
            end
         end
      end
   end
end



socket = TCPSocket.open( "localhost", 2000 )
Client.new( socket )
