require 'socket' 
SIZE = 1024 * 1024 * 10 #Mb

TCPSocket.open('127.0.0.1', 12345) do |socket| 
   File.open('input', 'rb') do |file| 
      i = 0
      [10240].pack("N").unpack("N")
      while chunk = file.read(SIZE)
          [i].pack("N")
          socket.write(chunk)
          i+=1 
      end 
   end 
end 
