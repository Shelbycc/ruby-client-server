require 'socket' 
require 'benchmark' 
SIZE = 1024 * 1024 * 10 #Mb

server = TCPServer.new("127.0.0.1", 12345) 
puts "Server listening..."    
client = server.accept  

time = Benchmark.realtime do
    File.open('output', 'wb') do |file| 
    while chunk = client.read(SIZE+4) 
        file.write(chunk) 
    end 
    end 
end 

file_size = File.size('output')/1024/1024 
puts "Time elapsed: #{time}. Transferred #{file_size} MB. Transfer per second: #{file_size/time} MB" and exit 
